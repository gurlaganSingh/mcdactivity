import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class McdTestCases {
	final String CHROMEDRIVER_LOCATION = "/Users/gurlaganbhullar/Desktop/chromedriver";
	final String URL_TO_TEST = "https://www.mcdonalds.com/ca/en-ca.html";
	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", CHROMEDRIVER_LOCATION);
	    driver = new ChromeDriver();
		
		driver.get(URL_TO_TEST);
		WebElement addCancel = driver.findElement(By.cssSelector("a.exit"));
//		// b. Click the button
		addCancel.click();
	
	}

	@After
	public void tearDown() throws Exception {
		//  Close the browser
		Thread.sleep(2000);
		driver.close();
	}
	@Test
	public void testCase1() {
		
		//TestCase 1 
		String expectedValue = driver.findElement(By.className("click-before-outline")).getText();
		//print the expected value
		//System.out.println(count);
		String originalValue = "Subscribe to My McD’s®";
		System.out.println("The Original value: "+ originalValue);
		
		assertEquals(originalValue, expectedValue);
		
		
	}
	@Test
	public void testCase2() {
		//Exit the pop-up add
		

		
		WebElement usernameBox = driver.findElement(By.id("firstname2"));
		// 4b. Put the user name in there
		usernameBox.sendKeys("gurlagan");

	
		WebElement emailBox = driver.findElement(By.id("email2"));
		// 5b. Put the password in there
		emailBox.sendKeys("satinder@gmail.com");
		
		WebElement postalCode = driver.findElement(By.id("postalcode2"));
		
		postalCode.sendKeys("L6Y");
	

		WebElement subcribeButton = driver.findElement(By.id("g-recaptcha-btn-2"));
		
		subcribeButton.click();
		
		// waiting for the captcha to be visible
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(
		ExpectedConditions.visibilityOfElementLocated(By.id("recaptcha-verify-button")));
		
		
		WebElement verifyButtonPresses = driver.findElement(By.cssSelector("#recaptcha-verify-button"));
		
	
		verifyButtonPresses.click();
		
		
		
		
	
	}
	@Test
	public void testCase3() {
		WebElement addCancel = driver.findElement(By.cssSelector("a.exit"));

		addCancel.click(); 
		
		WebElement subcribeButton = driver.findElement(By.id("g-recaptcha-btn-2"));
		
		subcribeButton.click();
		
		// waiting for the error to be visible
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(
		ExpectedConditions.visibilityOfElementLocated(By.id("firstname2-error")));
		
		
		
		//checking if the expected output is equal to actual output only when captcha does not shows
		String firstNameError = driver.findElement(By.id("firstname2-error")).getText();
		String originalFirstNameValue = "This field is required";
		
		String emailError = driver.findElement(By.id("email2-error")).getText();
		String originalEmailValue = "Invalid email address.";

		String postalCodeError = driver.findElement(By.id("postalcode2-error")).getText();
		String originalPostalCodeValue = "Invalid postal code.";
		
		assertEquals(firstNameError,originalFirstNameValue );
		
		assertEquals(emailError,originalEmailValue );
		
		assertEquals(originalEmailValue, originalPostalCodeValue);
		
		
		
		
	}
		

		
		
		
		
	


}
